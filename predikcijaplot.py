import matplotlib.pyplot as plt

Ax = []
Ay = []
Bx = []
By = []
Cx = []
Cy = []

with open('zad7-dataset.txt') as f, open('predikcije.txt') as p:
    pl = [float(x) for x in p.readline().strip().split(',')]
    for (dataset, predikcija) in zip(f.readlines(), pl):
        l = [float(x) for x in dataset.strip().split('\t')]
        if predikcija == 0:
            x = Ax
            y = Ay
        elif predikcija == 1:
            x = Bx
            y = By
        elif predikcija == 2:
            x = Cx
            y = Cy
        x.append(l[0])
        y.append(l[1])

wx = []
wy = []
sx = []
sy = []
with open('dobri-parametri.txt') as p:
    pl = [float(x) for x in p.readline().strip().split(',')]
    for i in range(0, 32, 4):
        wx.append(pl[i])
    for i in range(2, 32, 4):
        wy.append(pl[i])
    for i in range(1, 32, 4):
        sx.append(pl[i])
    for i in range(3, 32, 4):
        sy.append(pl[i])



plt.scatter(Ax, Ay, marker = 'o', label = 'A')
plt.scatter(Bx, By, marker = 'x', label = 'B')
plt.scatter(Cx, Cy, marker = '^', label = 'C')
plt.scatter(wx, wy, marker = 's', label = 'w')
plt.legend()
plt.show()

print(sx)
print(sy)