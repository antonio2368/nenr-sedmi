import matplotlib.pyplot as plt

Ax = []
Ay = []
Bx = []
By = []
Cx = []
Cy = []

with open('zad7-dataset.txt') as f:
    for line in f.readlines():
        l = [float(x) for x in line.strip().split('\t')]
        if l[2] == 1:
            x = Ax
            y = Ay
        elif l[3] == 1:
            x = Bx
            y = By
        elif l[4] == 1:
            x = Cx
            y = Cy
        x.append(l[0])
        y.append(l[1])

plt.scatter(Ax, Ay, marker = 'o', label = 'A')
plt.scatter(Bx, By, marker = 'x', label = 'B')
plt.scatter(Cx, Cy, marker = '^', label = 'C')
plt.legend()
plt.show()
            
        