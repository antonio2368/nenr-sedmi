import matplotlib.pyplot as plt
import numpy as np

x = np.linspace(-8, 10, 500)

funkcija = lambda x, s: 1 / (1 + abs(x - 2) / abs(s))

plt.plot(x, funkcija(x, 1), label='s = 1')
plt.plot(x, funkcija(x, 0.25), label='s = 0.25')
plt.plot(x, funkcija(x, 4), label='s = 4')
plt.legend()
plt.show()