package hr.fer;

import hr.fer.genetski.operations.crossover.*;
import hr.fer.genetski.operations.mutation.CombinedMutation;
import hr.fer.genetski.operations.mutation.IMutation;
import hr.fer.genetski.operations.mutation.NoiseMutation;
import hr.fer.genetski.operations.mutation.ReplaceNoiseMutation;
import hr.fer.genetski.operations.selection.ProportionalRandomSelection;
import hr.fer.genetski.optimizer.GenerationalFunctionOptimizer;
import hr.fer.genetski.optimizer.Generators;
import hr.fer.genetski.optimizer.IOptimizer;
import hr.fer.genetski.population.IParameterIndividual;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import java.util.function.Function;

public class Treniranje {
    public static void main(String[] args) {
        try {
            Dataset dataset = new Dataset("zad7-dataset.txt");
            NeuronskaMreza mreza = new NeuronskaMreza(2, 8, 3);

            Function<IParameterIndividual, Double> fitnessFunction =
                    i -> 1.0 / mreza.izracunajGresku(ListToArray(i.getParameters()), dataset);

            int populationSize = 50;

            IOptimizer optimizer = new GenerationalFunctionOptimizer(
                    populationSize,
                    0.01,
                    5,
                    50000,
                    fitnessFunction,
                    new ProportionalRandomSelection(),
                    new CombinedMutation(
                            new IMutation[] {
                            new NoiseMutation(0.5),
                            new NoiseMutation(1),
                            new ReplaceNoiseMutation(0.5)
                    }, new Double[] { 2.0, 2.0, 1.0 }),
                    new CombinedCrossover(
                            new SimpleArithmeticCrossover(10),
                            new SingleArithmeticRecombination(),
                            new DiscreteRecombination()
                    ),
                    Generators.gaussParameters(populationSize, mreza.brojParametara(), fitnessFunction)
            );

            IParameterIndividual individual = optimizer.optimize();

            try(PrintWriter writer = new PrintWriter("dobri-parametri.txt")) {
                for (int i = 0; i  < individual.parameterSize(); ++i) {
                    writer.write(Double.toString(individual.getParameter(i)));
                    if (i != individual.parameterSize() - 1) {
                        writer.write(',');
                    }
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static double[] ListToArray(List<Double> parameters) {
        double[] array = new double[parameters.size()];

        for (int i = 0; i < parameters.size(); ++i) {
            array[i] = parameters.get(i);
        }

        return array;
    }
}
