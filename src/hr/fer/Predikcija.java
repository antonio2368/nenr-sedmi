package hr.fer;

import java.io.IOException;
import java.io.PrintWriter;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;
import java.util.stream.Collectors;

public class Predikcija {
    public static void main(String[] args) throws IOException {
        double[] params = ListToArray(List.of(
                Files.readAllLines(Path.of("dobri-parametri.txt")).get(0).split("[,\n]"))
                                .stream()
                                .map(s -> Double.parseDouble(s))
                                .collect(Collectors.toList()));


        Dataset dataset = new Dataset("zad7-dataset.txt");
        NeuronskaMreza mreza = new NeuronskaMreza(2, 8, 3);

        try (PrintWriter writer = new PrintWriter("predikcije.txt")) {
            for (int i = 0; i < dataset.brojPodataka(); ++i) {
                double[] izlaz = mreza.izracunajIzlaz(params, dataset.dohvatiPodatak(i));
                for (int j = 0; j < izlaz.length; ++j) {
                    if (izlaz[j] > 0.5) {
                        writer.write(Integer.toString(j));
                        break;
                    }
                }
                if (i != dataset.brojPodataka() - 1) {
                    writer.write(',');
                }
            }
        }

    }

    private static double[] ListToArray(List<Double> parameters) {
        double[] array = new double[parameters.size()];

        for (int i = 0; i < parameters.size(); ++i) {
            array[i] = parameters.get(i);
        }

        return array;
    }
}
