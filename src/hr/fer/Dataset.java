package hr.fer;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

public class Dataset {
    private List<Data> dataset;

    public Dataset(String file) throws IOException {
        dataset = new ArrayList<>();
        Files.readAllLines(Paths.get(file))
                .stream()
                .forEach(l -> {
                    String[] params = l.split("\t");
                    int[] output = new int[3];
                    output[0] = Integer.parseInt(params[2]);
                    output[1] = Integer.parseInt(params[3]);
                    output[2] = Integer.parseInt(params[4]);
                    dataset.add(new Data(Double.parseDouble(params[0]), Double.parseDouble(params[1]), output));
                });
    }

    public int brojPodataka() {
        return dataset.size();
    }

    public Data dohvatiPodatak(int i) {
        // TODO provjeri index
        return dataset.get(i);
    }

}
