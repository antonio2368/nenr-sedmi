package hr.fer.genetski.operations.crossover;

import hr.fer.genetski.population.IParameterIndividual;

import java.util.Random;


public class CombinedCrossover implements ICrossover {
    private ICrossover[] crossovers;

    public CombinedCrossover(ICrossover... crossovers) {
        this.crossovers = crossovers;
    }

    @Override
    public IParameterIndividual crossIndividuals(IParameterIndividual firstIndividual, IParameterIndividual secondIndividual) {
        return crossovers[new Random().nextInt(crossovers.length)].crossIndividuals(firstIndividual, secondIndividual);
    }
}
