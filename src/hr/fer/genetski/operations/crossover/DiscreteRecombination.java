package hr.fer.genetski.operations.crossover;

import hr.fer.genetski.population.IParameterIndividual;
import hr.fer.genetski.population.ParameterIndividual;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class DiscreteRecombination implements ICrossover {
    @Override
    public IParameterIndividual crossIndividuals(IParameterIndividual firstIndividual, IParameterIndividual secondIndividual) {
        int parameterSize = firstIndividual.parameterSize();

        List<Double> parameters = new ArrayList<>();

        Random random = new Random();

        for (int i = 0; i < parameterSize; ++i) {
            double p = random.nextDouble();
            parameters.add(p < 0.5 ? firstIndividual.getParameter(i) : secondIndividual.getParameter(i));
        }

        return new ParameterIndividual(parameters);
    }
}
