package hr.fer.genetski.operations.crossover;
import hr.fer.genetski.population.IParameterIndividual;
import hr.fer.genetski.population.ParameterIndividual;

import java.util.List;
import java.util.stream.Collectors;

public class SimpleArithmeticCrossover implements ICrossover {
    private int limit;

    public SimpleArithmeticCrossover(int limit) {
        this.limit = limit;
    }

    @Override
    public IParameterIndividual crossIndividuals(IParameterIndividual firstIndividual, IParameterIndividual secondIndividual) {
        // TODO add valdiation for sizes
        int parameterSize = firstIndividual.parameterSize();
        List<Double> parameters = firstIndividual.getParameters().stream().limit(limit).collect(Collectors.toList());

        for (int i = limit; i < parameterSize; ++i) {
            parameters.add((firstIndividual.getParameter(i) + secondIndividual.getParameter(i)) / 2.0);
        }

        return new ParameterIndividual(parameters);
    }
}
