package hr.fer.genetski.operations.crossover;

import hr.fer.genetski.population.IParameterIndividual;
import hr.fer.genetski.population.ParameterIndividual;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class SingleArithmeticRecombination implements ICrossover {
    @Override
    public IParameterIndividual crossIndividuals(IParameterIndividual firstIndividual, IParameterIndividual secondIndividual) {
        Random random = new Random();
        int combinedParameter = random.nextInt(firstIndividual.parameterSize());

        List<Double> parameters = new ArrayList<>();
        for (int i = 0; i < firstIndividual.parameterSize(); ++i) {
            parameters.add(i == combinedParameter ?
                    (firstIndividual.getParameter(i) + secondIndividual.getParameter(i)) / 2.0
                    : firstIndividual.getParameter(i));
        }

        return new ParameterIndividual(parameters);
    }
}
