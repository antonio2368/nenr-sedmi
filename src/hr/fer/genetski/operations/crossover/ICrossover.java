package hr.fer.genetski.operations.crossover;

import hr.fer.genetski.population.IParameterIndividual;

public interface ICrossover {
    IParameterIndividual crossIndividuals(IParameterIndividual firstIndividual, IParameterIndividual secondIndividual);
}
