package hr.fer.genetski.operations.selection;

import hr.fer.genetski.population.IParameterIndividual;
import hr.fer.genetski.population.IPopulation;

import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

public class WorstSelection implements ISelection {
    @Override
    public List<IParameterIndividual> selectIndividuals(IPopulation population, int num) {
        return population.getIndividuals()
                .stream()
                .sorted(Comparator.comparingDouble(i -> i.getFitness()))
                .limit(num)
                .collect(Collectors.toList());
    }

    @Override
    public IParameterIndividual selectIndividual(IPopulation population) {
        return population.getIndividuals()
                .stream()
                .min(Comparator.comparingDouble(i -> i.getFitness()))
                .orElse(null);
    }
}
