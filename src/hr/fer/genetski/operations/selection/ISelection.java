package hr.fer.genetski.operations.selection;

import hr.fer.genetski.population.IParameterIndividual;
import hr.fer.genetski.population.IPopulation;

import java.util.List;

public interface ISelection {
    IParameterIndividual selectIndividual(IPopulation population);
    List<IParameterIndividual> selectIndividuals(IPopulation population, int num);
}
