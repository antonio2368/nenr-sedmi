package hr.fer.genetski.operations.selection;


import hr.fer.genetski.population.IParameterIndividual;
import hr.fer.genetski.population.IPopulation;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;

public class ProportionalRandomSelection implements ISelection {
    @Override
    public IParameterIndividual selectIndividual(IPopulation population) {
        List<IParameterIndividual> individuals = selectIndividuals(population, 1);
        return individuals.size() == 0 ? null : individuals.get(0);
    }

    @Override
    public List<IParameterIndividual> selectIndividuals(IPopulation population, int num) {
        Random random = new Random();
        double sum = population.getIndividuals().stream()
                                                .map(i -> i.getFitness())
                                                .reduce(0.0, (acc, curr) -> acc + curr);
        List<Double> probabilities = population.getIndividuals().stream().map(i -> i.getFitness() / sum).collect(Collectors.toList());

        List<IParameterIndividual> individuals = new ArrayList<>();

        while (individuals.size() != num) {
            double prob = random.nextDouble();
            double lastProb = 0;
            for (int i = 0; i < probabilities.size(); ++i) {
                lastProb = i == probabilities.size() - 1 ? 1 : lastProb + probabilities.get(i);
                if (prob < lastProb) {
                    individuals.add(population.getIndividual(i));
                    break;
                }
            }
        }

        return individuals;
    }
}
