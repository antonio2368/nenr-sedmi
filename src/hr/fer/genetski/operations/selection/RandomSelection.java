package hr.fer.genetski.operations.selection;

import hr.fer.genetski.population.IParameterIndividual;
import hr.fer.genetski.population.IPopulation;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;

public class RandomSelection implements ISelection {

    @Override
    public List<IParameterIndividual> selectIndividuals(IPopulation population, int num) {
        Random random = new Random();
        List<IParameterIndividual> copy = new ArrayList<>(population.getIndividuals());
        Collections.shuffle(copy);
        return copy.stream()
                .limit(num)
                .collect(Collectors.toList());
    }

    @Override
    public IParameterIndividual selectIndividual(IPopulation population) {
        Random random = new Random();
        return population.getIndividual(random.nextInt(population.getPopulationSize()));
    }
}
