package hr.fer.genetski.operations.mutation;

import hr.fer.genetski.population.IParameterIndividual;

import java.util.Arrays;
import java.util.Random;
import java.util.stream.Stream;

public class CombinedMutation implements IMutation {
    private double[] probabilities;
    private IMutation[] mutations;

    public CombinedMutation(IMutation[] mutations,
                            Double[] weights) {

        this.mutations = mutations;
        double sum = Stream.of(weights).reduce(0.0, (acc, curr) -> acc + curr);
        double currentProbability = 0;

        probabilities = new double[3];

        for (int i = 0; i < 3; ++i) {
            probabilities[i] = i == 2 ? 1 : currentProbability + weights[i] / sum;
            currentProbability = probabilities[i];
        }
    }

    @Override
    public IParameterIndividual mutateIndividual(IParameterIndividual individual, double mutationProbability) {
        Random random = new Random();
        double p = random.nextDouble();

        if (p < probabilities[0]) {
            return mutations[0].mutateIndividual(individual, mutationProbability);
        } else if (p < probabilities[1]) {
            return mutations[1].mutateIndividual(individual, mutationProbability);
        } else {
            return mutations[2].mutateIndividual(individual, mutationProbability);
        }
    }
}
