package hr.fer.genetski.operations.mutation;

import hr.fer.genetski.population.IParameterIndividual;

public interface IMutation {
    IParameterIndividual mutateIndividual(IParameterIndividual individual, double mutationProbability);
}
