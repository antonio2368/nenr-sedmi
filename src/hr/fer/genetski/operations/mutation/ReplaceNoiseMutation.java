package hr.fer.genetski.operations.mutation;

import hr.fer.genetski.population.IParameterIndividual;
import hr.fer.genetski.population.ParameterIndividual;

import java.util.Random;
import java.util.stream.Collectors;

public class ReplaceNoiseMutation implements IMutation {
    private double standardDeviation;

    public ReplaceNoiseMutation(double standardDeviation) {
        this.standardDeviation = standardDeviation;
    }

    @Override
    public IParameterIndividual mutateIndividual(IParameterIndividual individual, double mutationProbability) {
        Random random = new Random();

        return new ParameterIndividual(individual.getParameters()
                .stream()
                .map(p -> {
                    double prob = random.nextDouble();
                    if (prob < mutationProbability) {
                       return random.nextGaussian() * standardDeviation;
                    }
                    return p;
                })
                .collect(Collectors.toList()));
    }
}
