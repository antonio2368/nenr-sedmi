package hr.fer.genetski.population;

import java.util.List;

public class ParameterIndividual implements IParameterIndividual {
    private List<Double> parameters;
    private double fitness;

    public ParameterIndividual(List<Double> parameters) {
        this.parameters = parameters;
    }

    public double getParameter(int i) {
        // TODO add index validation
        return parameters.get(i);
    }

    @Override
    public double getFitness() {
        return fitness;
    }

    public void setFitness(double fitness) {
        this.fitness = fitness;
    }

    public List<Double> getParameters() {
        return parameters;
    }

    public int parameterSize() {
        return parameters.size();
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("(");
        for (int i = 0; i < parameterSize(); ++i) {
            sb.append(parameters.get(i));
            if (i != parameterSize() - 1) {
                sb.append(", ");
            }
        }
        sb.append(")");

        return sb.toString();
    }
}
