package hr.fer.genetski.population;

import java.util.List;

public interface IPopulation {
    int getPopulationSize();
    IParameterIndividual getIndividual(int i);
    List<IParameterIndividual> getIndividuals();
    void addIndividual(IParameterIndividual individual);
    void removeIndividual(IParameterIndividual individual);
}
