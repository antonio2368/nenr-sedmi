package hr.fer.genetski.population;

import java.util.List;

public interface IParameterIndividual {
    double getParameter(int i);
    List<Double> getParameters();
    int parameterSize();
    double getFitness();
    void setFitness(double fitness);
}
