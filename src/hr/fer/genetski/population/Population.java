package hr.fer.genetski.population;

import java.util.List;

public class Population implements IPopulation {
    private List<IParameterIndividual> individuals;

    public Population(List<IParameterIndividual> individuals) {
        this.individuals = individuals;
    }

    public int getPopulationSize() {
        return individuals.size();
    }

    @Override
    public void addIndividual(IParameterIndividual individual) {
        individuals.add(individual);
    }

    @Override
    public void removeIndividual(IParameterIndividual individual) {
        individuals.remove(individual);
    }

    @Override
    public IParameterIndividual getIndividual(int i) {
        // TODO add validation for index
        return individuals.get(i);
    }

    @Override
    public List<IParameterIndividual> getIndividuals() {
        return individuals;
    }

}