package hr.fer.genetski.optimizer;

import hr.fer.genetski.operations.crossover.ICrossover;
import hr.fer.genetski.operations.mutation.IMutation;
import hr.fer.genetski.operations.selection.BestSelection;
import hr.fer.genetski.operations.selection.ISelection;
import hr.fer.genetski.population.IParameterIndividual;
import hr.fer.genetski.population.IPopulation;
import hr.fer.genetski.population.Population;

import java.util.List;
import java.util.function.Function;
import java.util.function.Supplier;

public class GenerationalFunctionOptimizer extends Optimizer {
    private int elitism;

    public GenerationalFunctionOptimizer(int populationSize,
                                         double mutationProbability,
                                         int elitism,
                                         int maxIter,
                                         Function<IParameterIndividual, Double> fitnessFunction,
                                         ISelection selection,
                                         IMutation mutation,
                                         ICrossover crossover,
                                         Supplier<IPopulation> generator) {
        super(populationSize, mutationProbability, maxIter, fitnessFunction, selection, mutation, crossover, generator);
        this.elitism = elitism;
    }

    @Override
    public IParameterIndividual optimize() {
        currentPopulation = generator.get();

        List<IParameterIndividual> newIndividuals;

        double bestFitness = Double.MIN_VALUE;

        for (int t = 0; t < maxIter && bestFitness < 10e7; ++t) {
            newIndividuals = new BestSelection().selectIndividuals(currentPopulation, elitism);

            while (newIndividuals.size() < populationSize) {
                List<IParameterIndividual> parents = selection.selectIndividuals(currentPopulation, 2);

                IParameterIndividual child = crossover.crossIndividuals(parents.get(0), parents.get(1));
                child = mutation.mutateIndividual(child, mutationProbability);
                child.setFitness(fitnessFunction.apply(child));
                newIndividuals.add(child);
            }

            currentPopulation = new Population(newIndividuals);
            double currentBest = fitnessFunction.apply(new BestSelection().selectIndividual(currentPopulation));

            if (currentBest > bestFitness) {
                bestFitness = currentBest;

                System.out.printf("%d - %.4f%n", t, bestFitness);
            }

        }

        return new BestSelection().selectIndividual(currentPopulation);
    }

}
