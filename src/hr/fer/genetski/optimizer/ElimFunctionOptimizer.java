package hr.fer.genetski.optimizer;

import hr.fer.genetski.operations.crossover.ICrossover;
import hr.fer.genetski.operations.mutation.IMutation;
import hr.fer.genetski.operations.selection.BestSelection;
import hr.fer.genetski.operations.selection.ISelection;
import hr.fer.genetski.operations.selection.WorstSelection;
import hr.fer.genetski.population.IParameterIndividual;
import hr.fer.genetski.population.IPopulation;
import hr.fer.genetski.population.Population;

import java.util.List;
import java.util.function.Function;
import java.util.function.Supplier;

public class ElimFunctionOptimizer extends Optimizer {
    private int mortality;

    public ElimFunctionOptimizer(int populationSize,
                                         double mutationProbability,
                                         int mortality,
                                         int maxIter,
                                         Function<IParameterIndividual, Double> fitnessFunction,
                                         ISelection selection,
                                         IMutation mutation,
                                         ICrossover crossover,
                                         Supplier<IPopulation> generator) {
        super(populationSize, mutationProbability, maxIter, fitnessFunction, selection, mutation, crossover, generator);
        this.mortality = mortality;
    }

    @Override
    public IParameterIndividual optimize() {
        currentPopulation = generator.get();

        double bestFitness = Double.MIN_VALUE;

        for (int t = 0; t < maxIter; ++t) {
            for (int i = 0; i < mortality; ++i) {
                List<IParameterIndividual> selected = selection.selectIndividuals(currentPopulation, 3);
                IParameterIndividual worst = new WorstSelection().selectIndividual(new Population(selected));

                selected.remove(worst);
                currentPopulation.removeIndividual(worst);

                IParameterIndividual child = crossover.crossIndividuals(selected.get(0), selected.get(1));
                child = mutation.mutateIndividual(child, mutationProbability);
                child.setFitness(fitnessFunction.apply(child));
                currentPopulation.addIndividual(child);
            }

            double currentBest = fitnessFunction.apply(new BestSelection().selectIndividual(currentPopulation));

            if (currentBest > bestFitness) {
                bestFitness = currentBest;

                System.out.printf("%d - %.4f%n", t, bestFitness);
            }
        }

        return new BestSelection().selectIndividual(currentPopulation);
    }

}
