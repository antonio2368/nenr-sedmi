package hr.fer.genetski.optimizer;
import hr.fer.genetski.population.IParameterIndividual;
import hr.fer.genetski.population.IPopulation;
import hr.fer.genetski.population.ParameterIndividual;
import hr.fer.genetski.population.Population;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.function.Function;
import java.util.function.Supplier;

public class Generators {
    public static Supplier<IPopulation> generateParameters(int populationSize, int paramNum, double low, double high,
                                                           Function<IParameterIndividual, Double> fitnessFunction) {
        return () -> {
            List<IParameterIndividual> individuals = new ArrayList<>();

            Random random = new Random();
            for (int i = 0; i < populationSize; ++i) {
                List<Double> parameters = new ArrayList<>();

                for (int j = 0; j < paramNum; ++j) {
                    parameters.add(low + (high - low) * random.nextDouble());
                }

                IParameterIndividual individual = new ParameterIndividual(parameters);
                individual.setFitness(fitnessFunction.apply(individual));
                individuals.add(individual);
            }

            return new Population(individuals);
        };
    }

    public static Supplier<IPopulation> gaussParameters(int populationSize, int paramNum, Function<IParameterIndividual, Double> fitnessFunction) {
        return () -> {
            List<IParameterIndividual> individuals = new ArrayList<>();

            Random random = new Random();
            for (int i = 0; i < populationSize; ++i) {
                List<Double> parameters = new ArrayList<>();

                for (int j = 0; j < paramNum; ++j) {
                    parameters.add(random.nextGaussian());
                }

                IParameterIndividual individual = new ParameterIndividual(parameters);
                individual.setFitness(fitnessFunction.apply(individual));
                individuals.add(individual);
            }

            return new Population(individuals);
        };
    }
}
