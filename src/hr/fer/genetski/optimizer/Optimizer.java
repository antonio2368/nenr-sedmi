package hr.fer.genetski.optimizer;

import hr.fer.genetski.operations.crossover.ICrossover;
import hr.fer.genetski.operations.mutation.IMutation;
import hr.fer.genetski.operations.selection.ISelection;
import hr.fer.genetski.population.IParameterIndividual;
import hr.fer.genetski.population.IPopulation;

import java.util.function.Function;
import java.util.function.Supplier;

public abstract class Optimizer implements IOptimizer {
    protected int populationSize;
    protected double mutationProbability;
    protected ISelection selection;
    protected IMutation mutation;
    protected ICrossover crossover;
    protected int maxIter;
    protected Function<IParameterIndividual, Double> fitnessFunction;

    protected IPopulation currentPopulation;
    protected Supplier<IPopulation> generator;

    public Optimizer(int populationSize,
                     double mutationProbability,
                     int maxIter,
                     Function<IParameterIndividual, Double> fitnessFunction,
                     ISelection selection,
                     IMutation mutation,
                     ICrossover crossover,
                     Supplier<IPopulation> generator) {
        this.populationSize = populationSize;
        this.mutationProbability = mutationProbability;
        this.maxIter = maxIter;

        if (selection == null) {
            throw new IllegalArgumentException("ISelection object cannot be null.");
        }
        this.selection = selection;

        if (mutation == null) {
            throw new IllegalArgumentException("Mutation object cannot be null.");
        }

        this.mutation = mutation;

        if (crossover == null) {
            throw new IllegalArgumentException("Crossover object cannot be null.");
        }
        this.crossover = crossover;

        if (fitnessFunction == null) {
            throw new IllegalArgumentException("Fintes function cannot be null");
        }

        this.fitnessFunction = fitnessFunction;

        if (generator == null) {
            throw new IllegalArgumentException("Generator cannot be null");
        }

        this.generator = generator;
    }

    public abstract IParameterIndividual optimize();

}
