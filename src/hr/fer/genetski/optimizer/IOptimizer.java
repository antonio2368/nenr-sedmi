package hr.fer.genetski.optimizer;

import hr.fer.genetski.population.IParameterIndividual;

public interface IOptimizer {
    IParameterIndividual optimize();
}
