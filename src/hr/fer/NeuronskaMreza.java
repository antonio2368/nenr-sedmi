package hr.fer;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class NeuronskaMreza {
    private double[] mreza;
    private List<Integer> velicinaSlojeva;

    public NeuronskaMreza(Integer... velicinaSlojeva) {
        this.velicinaSlojeva = Arrays.asList(velicinaSlojeva);
        int brojNeurona = this.velicinaSlojeva.stream().reduce(0, (acc, curr) -> curr += acc);
        mreza = new double[brojNeurona];
    }

    public int brojParametara() {
        int sum = 0;

        sum += velicinaSlojeva.get(0) * velicinaSlojeva.get(1) * 2;

        for (int i = 1; i < velicinaSlojeva.size() - 1; ++i) {
            sum += velicinaSlojeva.get(i + 1) * (velicinaSlojeva.get(i) + 1);
        }

        return sum;
    }

    public double[] izracunajIzlaz(double[] parametri, Data ulaz) {
        int pocetakPrethodnogSloja;
        int velicinaPrethodnogSloja;
        int pocetakTrenutnogSloja;
        int velicinaTrenutnogSloja;

        // postavi neurone ulaznog sloja
        mreza[0] = ulaz.getX();
        mreza[1] = ulaz.getY();

        int trenutniParametar = 0;

        // postavi neurone sloja tipa 1
        for (int i = 2; i < 2 + velicinaSlojeva.get(1); ++i) {
            double sum = 0;
            for (int j = 0; j < 2; ++j) {
                sum += Math.abs(mreza[j] - parametri[trenutniParametar]) / Math.abs(parametri[trenutniParametar + 1]);
                trenutniParametar += 2;
            }

            mreza[i] = 1.0 / (1.0 + sum);
        }

        pocetakPrethodnogSloja = 2;
        velicinaPrethodnogSloja = velicinaSlojeva.get(1);
        // postavi izlaze ostalih slojeva
        for (int i = 2; i < velicinaSlojeva.size(); ++i) {
            pocetakTrenutnogSloja = pocetakPrethodnogSloja + velicinaPrethodnogSloja;
            velicinaTrenutnogSloja = velicinaSlojeva.get(i);

            double[] bias = new double[velicinaTrenutnogSloja];
            for (int j = 0; j < velicinaTrenutnogSloja; ++j, ++trenutniParametar) {
                bias[j] = parametri[trenutniParametar];
            }

            for (int k = pocetakTrenutnogSloja; k < pocetakTrenutnogSloja + velicinaTrenutnogSloja; ++k) {
                double sum = 0;
                for (int j = pocetakPrethodnogSloja; j < pocetakPrethodnogSloja + velicinaPrethodnogSloja; ++j, ++trenutniParametar) {
                    sum += parametri[trenutniParametar] * mreza[j];
                }
                mreza[k] = sigmoida(sum + bias[k - pocetakTrenutnogSloja]);
            }

            pocetakPrethodnogSloja = pocetakTrenutnogSloja;
            velicinaPrethodnogSloja = velicinaTrenutnogSloja;
        }

        double[] output = new double[velicinaPrethodnogSloja];

        for (int i = pocetakPrethodnogSloja; i < pocetakPrethodnogSloja + velicinaPrethodnogSloja; ++i) {
            output[i - pocetakPrethodnogSloja] = mreza[i];
        }

        return output;
    }

    public double izracunajGresku(double[] parametri, Dataset dataset) {
        double greska = 0.0;

        for (int i = 0; i < dataset.brojPodataka(); ++i) {
            Data data = dataset.dohvatiPodatak(i);
            double[] output = izracunajIzlaz(parametri, data);
            int[] correct = data.getOutput();
            for (int j = 0; j < output.length; ++j) {
                greska += Math.pow(output[j] - correct[j], 2);
            }
        }

        return greska / dataset.brojPodataka();

    }

    private double sigmoida(double x) {
        return 1.0 / (1 + Math.exp(-x));
    }
}
