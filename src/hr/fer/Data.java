package hr.fer;

public class Data {
    private double x;
    private double y;

    private int[] output;

    public Data(double x, double y, int[] output) {
        this.x = x;
        this.y = y;
        this.output = output;
    }

    public double getX() {
        return x;
    }

    public void setX(double x) {
        this.x = x;
    }

    public double getY() {
        return y;
    }

    public void setY(double y) {
        this.y = y;
    }

    public int[] getOutput() {
        return output;
    }

    public void setOutput(int[] output) {
        this.output = output;
    }
}
