package test;

import hr.fer.Data;
import hr.fer.Dataset;

import java.io.IOException;

public class DatasetTest {
    public static void main(String[] args) {
        try {
            Dataset dataset = new Dataset("zad7-dataset.txt");

            System.out.println("Broj podataka: " + dataset.brojPodataka());
            for (int i = 0; i < dataset.brojPodataka(); ++i) {
                Data podatak = dataset.dohvatiPodatak(i);
                System.out.printf("%f %f - %d %d %d%n", podatak.getX(), podatak.getY(),
                        podatak.getOutput()[0], podatak.getOutput()[1], podatak.getOutput()[2]);
            }
        } catch (IOException e) {
            System.err.println("Couldn't open the file");
        }

    }
}
