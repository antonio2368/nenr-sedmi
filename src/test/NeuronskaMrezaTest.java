package test;

import hr.fer.Dataset;
import hr.fer.NeuronskaMreza;

import java.io.IOException;
import java.util.Random;

public class NeuronskaMrezaTest {
    public static void main(String[] args) throws IOException {
        NeuronskaMreza mreza = new NeuronskaMreza(2, 8, 3);

        System.out.println("Broj parametara: " + mreza.brojParametara());

        double[] parametri = new double[mreza.brojParametara()];

        for (int i = 0; i < mreza.brojParametara(); ++i) {
            Random random = new Random();
            parametri[i] = 1;
        }

        Dataset dataset = new Dataset("zad7-dataset.txt");

        double[] result = mreza.izracunajIzlaz(parametri, dataset.dohvatiPodatak(0));

        for (int i = 0; i < 3; ++i) {
            System.out.printf("klasa%d - %f%n", i, result[i]);
        }

        System.out.println("Greska: " + mreza.izracunajGresku(parametri, dataset));
    }
}
